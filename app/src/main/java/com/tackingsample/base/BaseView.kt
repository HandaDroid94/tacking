package com.tackingsample.base

import okhttp3.ResponseBody

interface BaseView
{
    fun showLoader(isLoading: Boolean)
    fun apiFaliure()
    fun apiError(errorBody: ResponseBody?)
}

