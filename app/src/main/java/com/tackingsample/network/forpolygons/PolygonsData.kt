package com.tackingsample.network.forpolygons

import com.tackingsample.network.LocationData


class PolygonsData {

    var _id: String? = null
    var name: String? = null
    var polygon: String? = null
    var userId: String? = null
    var createdAt: String? = null
    var __v: Int? = null
}