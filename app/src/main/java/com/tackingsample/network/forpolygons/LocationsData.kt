package com.tackingsample.network.forpolygons

class LocationsData {
    var type: String? = null
    var coordinates: List<Float>? = null
}