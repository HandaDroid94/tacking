package com.tackingsample.network

class MapData {

    var _id: String? = null
    var location: LocationData? = null
    var battery: String? = null
    var bearing: String? = null
    var signal: String? = null
    var accuracy: String? = null
    var timeStamp: String? = null
    var __v: Int? = null
}