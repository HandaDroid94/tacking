package com.tackingsample.network

import com.tackingsample.network.forpolygons.PolygonsMain
import retrofit2.Call
import retrofit2.http.*


interface CoWorkerApi {

    //to register user
    @FormUrlEncoded
    @POST("users/registerUser")
    fun apiRegister(@FieldMap map: HashMap<String, String>): Call<SignUp>

    //to login user
    @FormUrlEncoded
    @POST("users/login")
    fun apiLogin(@FieldMap map: HashMap<String, String>): Call<SignUp>

    //to sync user
    @FormUrlEncoded
    @POST("users/syncDevice")
    fun apiSync(@Header("authorization") authorization: String,
                @FieldMap map: HashMap<String, String>): Call<SignUp>

    //to add polygon
    @FormUrlEncoded
    @POST("users/addPolygon")
    fun apiAddPolygon(@Header("authorization") authorization: String,
                @FieldMap map: HashMap<String, String>): Call<Any>


    //to get all polygons
    @POST("users/listPolygon")
    fun apiGetPolygons(@Header("authorization") authorization: String):
            Call<PolygonsMain>

    //to get latlng for polyline
    @FormUrlEncoded
    @POST("users/getRoute")
    fun apiGetRoute(@Header("authorization") authorization: String,
                @FieldMap map: HashMap<String, String>): Call<MapMain>


    /* //to register user
     @FormUrlEncoded
     @POST("device/getToken")
     fun apiRegister(@FieldMap map: HashMap<String, String>): Call<SignUp>

     //to register user
     @FormUrlEncoded
     @POST("device/updateLocation")
     fun apiUpdateLocation(@Header("authorization") authorization: String,
                           @Field("param") backendData: String): Call<SignUp>
 */
/*


    // to get login in app
    @GET("user/login")
    fun apiLogin(@QueryMap map: HashMap<String, String>):Call<SignUp>


    // to get event detail
    @GET("user/detailEvents")
    fun apiEventDetail(@Header("authorization") authorization: String,
                       @Query("key")key:String,
                       @Query("eventId")eventId:String):Call<EventDetailMain>




    // to get login in app
    @GET("user/chatSummary")
    fun apiGetChatListing(@Header("authorization") authorization: String,
                          @Query("key")key:String):Call<ChatSummary>




    // to get login in app
    @GET("user/getIndividualChat")
    fun apiGetOneToOneChat(@Header("authorization") authorization: String,
                           @QueryMap map: HashMap<String, String>):Call<ChatConversationMain>



    // to get liked people list
    @GET("user/getNewsLogs")
    fun apiGetLikedPeople(@Header("authorization") authorization: String,
                           @QueryMap map: HashMap<String, String>):Call<MembersMain>





    // to get comment listing
    @GET("user/getNewsLogs")
    fun apiGetCommentListing(@Header("authorization") authorization: String,
                           @QueryMap map: HashMap<String, String>):Call<CommentMain>




    // to get login in app
    @GET("user/getQuestions")
    fun apiGetQuestions(@Query("key") map:String):Call<Questions>




    // to get branches
    @FormUrlEncoded
    @POST("user/listBranches")
    fun apiGetBranches(@Field("key") map:String):Call<BranchList>



    // to get titles
    @FormUrlEncoded
    @POST("user/listTicketTitle")
    fun apiGetTitles(@Header("authorization") authorization: String,
                     @Field("key") map:String):Call<BranchList>



    // to get all tickets
    @FormUrlEncoded
    @POST("user/listTicket")
    fun apiGetTickets(@Header("authorization") authorization: String,
                     @Field("key") map:String):Call<TicketListing>




    // to get available packages
    @GET("user/listPackages")
    fun apiGetPackages(@QueryMap map:HashMap<String,String>):Call<PackagesMain>


    // to get my packages
    @GET("user/myPackages")
    fun apiGetMyPackages(@Header("authorization") authorization: String,
                         @QueryMap map:HashMap<String,String>):Call<PackagesMain>


    // to get my team
    @GET("user/myTeam")
    fun apiGetMyTeam(@Header("authorization") authorization: String,
                         @QueryMap map:HashMap<String,String>):Call<MyTeamMain>


    //for adding answer
    @FormUrlEncoded
    @POST("user/addEnquiry")
    fun apiAddAnswer(@FieldMap map: HashMap<String, String>): Call<SignUp>




    //forgot password
    @FormUrlEncoded
    @POST("user/forgotPassword")
    fun apiForgotPassword(@Field("key") key:String
                          ,@Field("email") map:String): Call<SignUp>




    //for removing team member
    @FormUrlEncoded
    @POST("user/removeUserFromPackage")
    fun apiRemoveMember(@Header("authorization") authorization: String,
                        @FieldMap map: HashMap<String, String>): Call<SignUp>






    //for send vistor card
    @FormUrlEncoded
    @POST("user/inviteMeetingGuest")
    fun apiGenerateVistorCard(@Header("authorization") authorization: String,
                        @FieldMap map: HashMap<String, String>): Call<GenerateQR>





    //for genrating ticket
    @FormUrlEncoded
    @POST("user/raiseTicket")
    fun apiRaiseTicket(@Header("authorization") authorization: String,
                        @FieldMap map: HashMap<String, String>): Call<AddTicketMain>



    //for offers
    @FormUrlEncoded
    @POST("user/OfferCategory")
    fun apiOfferCategories(@Header("authorization") authorization: String,
                        @FieldMap map: HashMap<String, String>): Call<OfferCategoryMain>



    //for offer listings
    @FormUrlEncoded
    @POST("user/listOffers")
    fun apiOfferListing(@Header("authorization") authorization: String,
                        @FieldMap map: HashMap<String, String>): Call<OfferCategoryMain>



    //for booking package
    @FormUrlEncoded
    @POST("user/assignPackageUser")
    fun apiBookPackage(@Header("authorization") authorization: String,
                       @FieldMap map: HashMap<String, String>): Call<SignUp>




    //for cancel order
    @FormUrlEncoded
    @POST("user/cancelOrder")
    fun apiCancelOrder(@Header("authorization") authorization: String,
                       @FieldMap map: HashMap<String, String>): Call<CancelOrder>



    //apiInviteUserToPackage
    @FormUrlEncoded
    @POST("user/inviteUserToPackage")
    fun apiInviteUserToPackage(@Header("authorization") authorization: String,
                       @FieldMap map: HashMap<String, String>): Call<SignUp>



    //for renew package
    @FormUrlEncoded
    @POST("user/renewPackage")
    fun apiRenewPackage(@Header("authorization") authorization: String,
                       @FieldMap map: HashMap<String, String>): Call<SignUp>



    //for checking update
    @FormUrlEncoded
    @POST("user/checkVersion")
    fun apiCheckVersion(@FieldMap map: HashMap<String, String>): Call<SignUp>



    //to register user
    @FormUrlEncoded
    @POST("user/register")
    fun apiRegister(@FieldMap map: HashMap<String, String>): Call<SignUp>


    //to get home data
    @FormUrlEncoded
    @POST("user/homeData")
    fun apiHomeData(@Header("authorization") authorization: String,
                    @Field("key")key:String): Call<SignUp>


    //to getting bookings
    @FormUrlEncoded
    @POST("user/getBookings")
    fun apiGetBooking(@Header("authorization") authorization: String,
                      @FieldMap map:HashMap<String,String>): Call<MyBookingMain>


    //to getting bookings
    @FormUrlEncoded
    @POST("user/addEditComment")
    fun apiPostComment(@Header("authorization") authorization: String,
                      @FieldMap map:HashMap<String,String>): Call<PostComment>


    //for cancel booking
    @FormUrlEncoded
    @POST("user/cancelBooking")
    fun apiCancelBooking(@Header("authorization") authorization: String,
                         @Field("key")key:String,
                         @Field("bookingId")bookingId:String): Call<SignUp>


    //for in app notifications
    @FormUrlEncoded
    @POST("user/getNotifications")
    fun apiGetNotifications(@Header("authorization") authorization: String,
                         @Field("key")key:String,
                         @Field("pageNo")bookingId:String): Call<NotificationMain>


    //for updating device token
    @FormUrlEncoded
    @POST("user/upDeviceToken")
    fun apiUpdateDeviceToken(@Header("authorization") authorization: String,
                         @Field("deviceToken")deviceToken:String): Call<SignUp>


    //for on off push notifications
    @FormUrlEncoded
    @POST("user/onOffNotification")
    fun apiOnOffNotification(@Header("authorization") authorization: String,
                         @Field("key")key:String,
                         @Field("status")status:Boolean): Call<SignUp>



    //for getting company list
    @FormUrlEncoded
    @POST("user/companyList")
    fun apiGetCompanyList(@Header("authorization") authorization: String,
                         @Field("key")key:String): Call<CompanyListMain>


    // for uploading image
    @Multipart
    @POST("api/uploadImage")
    fun apiUploadImage(@PartMap map:HashMap<String, RequestBody>): Call<UploadImage>



    //to get all members
    @FormUrlEncoded
    @POST("user/getMember")
    fun apiGetMembers(@Header("authorization") authorization: String,
                      @FieldMap map: HashMap<String, String>): Call<MembersMain>

   @FormUrlEncoded
   @POST("user/joinOrLeaveCommunity")
   fun apiJoinLeave(@Header("authorization") authorization: String,
                    @FieldMap map: HashMap<String, String>): Call<MembersMain>


   @FormUrlEncoded
   @POST("user/inviteMembers")
   fun apiInviteMembers(@Header("authorization") authorization: String,
                    @FieldMap map: HashMap<String, String>): Call<MembersMain>



   @FormUrlEncoded
   @POST("user/addAdmins")
   fun apiAddAdmins(@Header("authorization") authorization: String,
                    @FieldMap map: HashMap<String, String>): Call<MembersMain>



    //action===1-block,2-unblock,3-clear
   @FormUrlEncoded
   @POST("user/blockClearChat")
   fun apiClearChat(@Header("authorization") authorization: String,
                    @FieldMap map: HashMap<String, String>): Call<UploadImage>




   @FormUrlEncoded
   @POST("user/joinOrLeaveEvent")
   fun apiJoinOrLeaveEvent(@Header("authorization") authorization: String,
                    @FieldMap map: HashMap<String, String>): Call<UploadImage>



   //to get my list Communities
   @GET("user/listCommunities")
   fun apiGetCommunities(@Header("authorization") authorization: String,
//                         @Query ("key") key: String
                         @QueryMap map: HashMap<String, String>): Call<CommunityMain>



   @GET("user/communityDetail")
   fun apiGetCommunityDetails(@Header("authorization") authorization: String,
                              @Query ("key") key: String, @Query ("communityId") communityId: String): Call<Data>



   //to get my members
//    @GET("user/listMyMember")
//    fun apiGetMyMembers(@Header("authorization") authorization: String,
//                      @Query ("key") key: String): Call<MembersMain>

    //to get my members
    @GET("user/listMyMember")
    fun apiGetMyMembers(@Header("authorization") authorization: String,
                        @QueryMap map:HashMap<String,String>): Call<MembersMain>


     //to get my community
    @GET("user/listMyCommunities")
    fun apiGetMyCommunities(@Header("authorization") authorization: String,
                      @Query ("key") key: String): Call<CommunityListMain>


    //for adding new event
    @FormUrlEncoded
    @POST("user/addEditEvents")
    fun apiAddEvent(@Header("authorization") authorization: String,
                       @FieldMap map: HashMap<String, String>): Call<SignUp>




    //to like dislike
    @FormUrlEncoded
    @POST("user/likeNews")
    fun apiLikeDislike(@Header("authorization") authorization: String,
                       @FieldMap map: HashMap<String, String>): Call<SignUp>




    //to like dislike
    @FormUrlEncoded
    @POST("user/createCommunities")
    fun apiCreateCommunity(@Header("authorization") authorization: String,
                       @FieldMap map: HashMap<String, String>): Call<SignUp>



    //to get all stories
    @FormUrlEncoded
    @POST("user/getNews")
    fun apiGetPost(@Header("authorization") authorization: String,
                   @FieldMap map: HashMap<String, String>): Call<StoriesMain>

    @FormUrlEncoded
    @POST("user/deleteData")
    fun apiDelete(@Header("authorization") authorization: String,
                   @FieldMap map: HashMap<String, String>): Call<SignUp>

    @FormUrlEncoded
    @POST("user/reportData")
    fun apiReport(@Header("authorization") authorization: String,
                  @FieldMap map: HashMap<String, String>): Call<SignUp>


    //to get single post
    @FormUrlEncoded
    @POST("user/getNews")
    fun apiGetSinglePost(@Header("authorization") authorization: String,
                   @FieldMap map: HashMap<String, String>): Call<SinglePostData>




    //to get all stories
    @FormUrlEncoded
    @POST("user/getJobs")
    fun apiGetJobs(@Header("authorization") authorization: String,
                   @FieldMap map: HashMap<String, String>): Call<JobsMain>



    //to get all stories
    @GET("user/listEvents")
    fun apiGetEvents(@Header("authorization") authorization: String,
                   @QueryMap map: HashMap<String, String>): Call<EventListingMain>




    //for placing order
    @FormUrlEncoded
    @POST("user/orderProduct")
    fun apiPlaceOrder(@Header("authorization") authorization: String,
                      @FieldMap map: HashMap<String, String>): Call<OrderMain>




    //to post new story
    @FormUrlEncoded
    @POST("user/addEditNews")
    fun apiPost(@Header("authorization") authorization: String,
                @FieldMap map: HashMap<String, String>): Call<MembersMain>


    //to get product listing
    @FormUrlEncoded
    @POST("user/getProducts")
    fun apiGetProducts(@Header("authorization") authorization: String,
                       @FieldMap map: HashMap<String, String>): Call<OrderMain>



    //to post new job
    @FormUrlEncoded
    @POST("user/addEditJob")
    fun apiAddJob(@Header("authorization") authorization: String,
                  @FieldMap map: HashMap<String, String>): Call<MembersMain>


    //to post new job
    @FormUrlEncoded
    @POST("user/sendRequest")
    fun apiConnect(@Header("authorization") authorization: String,
                   @FieldMap map: HashMap<String, String>): Call<SignUp>


    //to edit user profile
    @FormUrlEncoded
    @POST("user/editProfile")
    fun apiEditProfile(@Header("authorization") authorization: String,
                       @FieldMap map: HashMap<String, String>): Call<SignUp>



    //for logout
    @POST("user/logout")
    fun apiLogout(@Header("authorization") authorization: String): Call<SignUp>


    //to get user profile
    @FormUrlEncoded
    @POST("user/userProfile")
    fun apiGetProfile(@Header("authorization") authorization: String,
                      @FieldMap map: HashMap<String, String>): Call<ProfileMain>



    //for change passsword
    @FormUrlEncoded
    @POST("user/changePassword")
    fun apiChangePassword(@Header("authorization") authorization: String,
                          @FieldMap map: HashMap<String, String>): Call<ProfileMain>



    //for getting services like meeting room
    @FormUrlEncoded
    @POST("user/getServices")
    fun apiGetServices(@Header("authorization") authorization: String,
                       @FieldMap map: HashMap<String, String>): Call<MeetingMain>


    //for booking room
    @FormUrlEncoded
    @POST("user/booking")
    fun apiBookRoom(@Header("authorization") authorization: String,
                    @FieldMap map: HashMap<String, String>): Call<BookMain>


    //to get orders
    @FormUrlEncoded
    @POST("user/myOrders")
    fun apiGetMyOrders(@Header("authorization") authorization: String,
                       @FieldMap map: HashMap<String, String>): Call<OrderHistoryMain>

*/

}

//62