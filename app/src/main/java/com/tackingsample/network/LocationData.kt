package com.tackingsample.network

class LocationData {

    var type: String? = null
    var coordinates: List<Float>? = null
}