package com.tackingsample.network

import com.google.gson.GsonBuilder
import com.tackingsample.utils.AppConstants
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

object RetrofitClient
{
    private var coWorkerApi: CoWorkerApi

    init
    {
        val retrofit = initRetrofitClient()
        coWorkerApi = retrofit.create(CoWorkerApi::class.java)
    }

    fun getApi(): CoWorkerApi = coWorkerApi

    private fun initRetrofitClient(): Retrofit
    {
        val client = OkHttpClient.Builder()
                .connectTimeout(30, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .build()

        val gson = GsonBuilder()
                .setLenient()
                .create()

        return Retrofit.Builder()
                .baseUrl(AppConstants.BASE_PATH)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build()
    }
}