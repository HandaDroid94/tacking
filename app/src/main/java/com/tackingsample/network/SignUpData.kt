package com.tackingsample.network

class SignUpData {

    var currentLocation: CurrentLocation? = null
    var name: String? = null
    var deviceToken: String? = null
    var accessToken: String? = null
    var password: String? = null
    var email: String? = null
    var phoneNo: String? = null
    var countryCode: String? = null
    var imei: String? = null
    var deviceType: String? = null
    var timeZone: String? = null
    var offSet: Int? = null
    var deviceId: Any? = null
    var usersPolygons: List<Any>? = null
    var version: String? = null
    var _id: String? = null
    var createdAt: String? = null
    var __v: Int? = null
}