package com.tackingsample.network

class SignUp {
    var statusCode: Int? = null
    var message: String? = null
    var data: SignUpData? = null
}