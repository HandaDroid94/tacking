package com.tackingsample.utils

import android.app.Activity
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.net.ConnectivityManager
import android.net.Uri
import android.provider.Settings
import android.support.design.widget.Snackbar
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import android.widget.Toast
import com.tackingsample.R
import okhttp3.ResponseBody
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*

// for activity
fun Context.hideKeyboard(activity: Activity) {
    val imm = activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
    var view = activity.currentFocus
    if (view == null) {
        view = View(activity)
    }
    imm.hideSoftInputFromWindow(view.windowToken, 0)
}


//for fragment
fun Context.hideKeyboard( view: View) {
    val imm = this.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
    imm.hideSoftInputFromWindow(view.windowToken, 0)
}

private fun getBitmapFromView(v: View): Bitmap {
    val bmp = Bitmap.createBitmap(v.measuredWidth, v.measuredHeight, Bitmap.Config.ARGB_8888)
    val c = Canvas(bmp)
    v.draw(c)
    return bmp
}


fun View.showSnack(resId: Int) = try {
    val snackbar = Snackbar.make(this, resId, Snackbar.LENGTH_LONG)
    val snackbarView = snackbar.view
    val textView = snackbarView.findViewById<View>(android.support.design.R.id.snackbar_text) as TextView
    textView.maxLines = 3
    /*  textView.setTextSize(TypedValue.COMPLEX_UNIT_SP,12f)*/
    snackbar.setAction(R.string.okay, View.OnClickListener { snackbar.dismiss() })
    snackbar.setActionTextColor(Color.parseColor("#FFFFFF"))
    snackbar.show()
} catch (e: Exception) {
    e.printStackTrace()
}

fun View.showSomeWWerror() {
    showSnack(R.string.sww_error)
}



private fun showMessageOKCancel(message: String, context: Context,
                                okListener: DialogInterface.OnClickListener) {
    AlertDialog.Builder(context)
            .setMessage(message)
            .setPositiveButton(R.string.ok, okListener)
            .setCancelable(false)
            .create()
            .show()
}



fun showPopUp(context: Context, string: String?) {
    showMessageOKCancel(string!!,context, DialogInterface.OnClickListener { dialog, which ->
        val reg_id = PrefsManager.get().getString(AppConstants.REG_ID, "")
        PrefsManager.get().removeAll()
        PrefsManager.get().save(AppConstants.REG_ID, reg_id)
        (context as AppCompatActivity).finishAffinity()
       // context.startActivity(Intent(context, LoginSignUpActivity::class.java))
    })

}

fun Context.displayApiError(errorBody: ResponseBody?, view: View?)
{
    val statusCode: Int
    val msg: String
    try {
        val jsonObject = JSONObject(errorBody?.string())
        statusCode = jsonObject.getInt("statusCode")
        msg = jsonObject.getString("message")
        if (statusCode == 401) {
            showPopUp(this, getString(R.string.bad_token_msg))
        } else
            view?.showSnack(msg)
    } catch (e: Exception) {
        e.printStackTrace()
    }
}

fun getFormatFromDate(date: Date, format: String): String {
    val sdf = SimpleDateFormat(format, Locale.US)
    try {
        return sdf.format(date)
    } catch (e: Exception) {
        e.printStackTrace()
        return ""
    }

}


fun parseDateLocal(date: String, inputFormat: String): Date {
    val inputParser = SimpleDateFormat(inputFormat, Locale.US)
    try {
        return inputParser.parse(date)
    } catch (e: java.text.ParseException) {
        return Date(0)
    }

}

fun View.showSnack( msg: String) {

    try {
        val snackbar = Snackbar.make(this, msg, Snackbar.LENGTH_LONG)
        val snackbarView = snackbar.view
        val textView = snackbarView.findViewById<View>(android.support.design.R.id.snackbar_text) as TextView
        textView.maxLines = 3
        snackbar.setAction(R.string.okay) { snackbar.dismiss() }/*
        snackbarView.setBackgroundColor(Color.parseColor("#F3331A48"))*/
        snackbar.setActionTextColor(Color.parseColor("#FFFFFF"))
        snackbar.show()
    } catch (e: Exception) {
        e.printStackTrace()
    }



}
fun View.showNetworkError() {
    showSnack( R.string.network_error)
}


fun Context.showMessageOKCancel( message: String) {
    android.support.v7.app.AlertDialog.Builder(this)
            .setMessage(message)
            .setPositiveButton("OK") { dialog, which ->
                val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
                val uri = Uri.fromParts("package", packageName, null)
                intent.data = uri
                (this as AppCompatActivity).startActivityForResult(intent, 100)
            }
            .setCancelable(false)
            .create()
            .show()
}




fun Context.isNetworkActive(): Boolean
{
    val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    val activeNetwork = connectivityManager.activeNetworkInfo
    return activeNetwork != null && activeNetwork.isConnectedOrConnecting
}

fun Context.isNetworkActiveWithMessage(): Boolean
{
    val isActive = isNetworkActive()

    if (!isActive)
        Toast.makeText(this, R.string.network_error, Toast.LENGTH_SHORT).show()

    return isActive
}





