package com.tackingsample.utils

import okhttp3.MediaType
import okhttp3.RequestBody
import java.io.File

object RetrofitUtils
{
    fun stringToRequestBody(string: String): RequestBody
            = RequestBody.create(MediaType.parse("text/plain"), string)

    fun imageToRequestBody(imageFile: File): RequestBody
            = RequestBody.create(MediaType.parse("image/*"), imageFile)

    fun imageToRequestBodyKey(parameterName: String, fileName: String): String
            = parameterName + "\"; filename=\"" + fileName
}