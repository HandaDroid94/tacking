package com.tackingsample.utils

object AppConstants
{
    const val BASE_PATH = "http://54.70.190.138:8003/"
    //const val BASE_PATH = "http://192.168.102.100:8000/"
    val APP_TOKEN = "app_token"
    val USER_ID = "userId"
    const val REG_ID = "deviceToken"
    //const  val USER_KEY = "dsddfdsf1522167316739"/*

    //live
    const  val merchant_Key = "D96nSgPK"
    const  val merchant_ID = "6309326"
    const  val merchant_SALT = "fGjTBWolDd"
    const  val debug_more = false


    //testing credentials
/*
    const  val merchant_Key = "40747T"
    const  val merchant_ID = "396132"
    const  val merchant_SALT = "cJHb2BC9"*///fGjTBWolDd

    const  val USER_KEY = "Ravish1535477258802"
    //const  val USER_KEY = "next571528733212441"
    const  val APP_PLAYSTORE = "https://play.google.com/store/apps/details?id=com.wehive.starthubnation"
    //for stackgeeks
    //const  val USER_KEY = "Spacejam1528136141213"//"spacejam1525523212778"
    val USER_DATA = "user"
    val LOGIN_STATUS = "status"
    val DATE_FORMAT_BACKEND = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
    const val MY_PERMISSIONS_REQUEST_STORAGE = 20
    const val REQ_CODE_GALLERY_IMAGE = 100
    const val REQ_CODE_CAMERA_IMAGE = 101

    // for socket
    val SENDER_ID="senderId"
    val RECEIVER_ID="receiverId"
    val MESSAGE="message"
    val SENT_AT="sentAt"
    const val NEWUSER = 1// if new user is registered
    const val ORDER = 2  // order accept n Reject
    const val LIKE = 3  // someone like your news
    const val JOB = 4 // your job is approved
    const val NEWS = 5 // your news is approved
    const val PUSH_TYPE = "type" // your news is approved
    const val PUSH_TITLE = "msg" // your news is approved
    const val PUSH_ID = "id" // your news is approved
    const val PROFILE_APPROVED = 6 // profile approved
    const val MESSAGE_TYPE = 7 // profile approved
    const val ADMIN_MSG = 8 //admin
    const val ADMIN_MSG_PRIORITY = 9 //important msg
    const val REQUEST_ACCEPT = 10 //accept reject  user request
    const val EVENT_INVITE = 11
    const val EVENT_ADMIN = 13
    const val ACCEPT_EVENT_INVITE = 12
    const val NEW_EVENT_ADDED = 14
    const val JOIN_LEAVE_COMMUNITY = 15
    const val PUSH_COMMENT = 16
    const val BOOKING_START = 17
}