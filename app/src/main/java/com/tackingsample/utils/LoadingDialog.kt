package com.tackingsample.utils

import android.app.Dialog
import android.content.Context
import android.view.View
import com.tackingsample.R

class LoadingDialog()
{
    private lateinit var dialog: Dialog

    constructor(context: Context) : this()
    {
        val dialogView = View.inflate(context, R.layout.progress_dialog, null)

        dialog = Dialog(context, R.style.CustomDialog)
        dialog.setContentView(dialogView)
        dialog.setCancelable(false)
    }

    private fun show()
    {
        if (!dialog.isShowing)
            dialog.show()
    }

    private fun dismiss()
    {
        if (dialog.isShowing)
            dialog.dismiss()
    }

    fun setLoading(isLoading: Boolean)
    {
        if (isLoading)
            show()
        else
            dismiss()
    }
}