package com.tackingsample.ui.signup

import com.tackingsample.base.BaseView
import com.tackingsample.network.SignUpData

/**
 * Created by prashant on 28/3/18.
 */
interface SignupContract {
    interface View: BaseView
    {
         fun apiSuccess(data: SignUpData?)
    }

    interface Presenter
    {
        fun apiSignUp(map:HashMap<String,String>)

    }
}