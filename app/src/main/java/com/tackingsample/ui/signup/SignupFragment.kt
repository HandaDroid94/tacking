package com.tackingsample.ui.signup

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import com.tackingsample.R
import com.tackingsample.network.SignUpData
import com.tackingsample.utils.*
import kotlinx.android.synthetic.main.fragment_signup.*
import okhttp3.ResponseBody

/**
 * Created by prashant on 28/3/18.
 */
class SignupFragment :AppCompatActivity(), SignupContract.View, View.OnClickListener {

    private val presenter= SignupPresenter()
    private var loadingBox: LoadingDialog?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.fragment_signup)
        init()
        setClickEvents()
    }





    private fun init() {
        presenter.attachView(this)
        loadingBox = LoadingDialog(this)

    }

    private fun setClickEvents() {
        tvSkip.setOnClickListener(this)
        tvSignUp.setOnClickListener(this)

    }

    override fun showLoader(isLoading: Boolean) {
        loadingBox?.setLoading(isLoading)
    }

    override fun apiFaliure() {
        tvSignUp?.showSomeWWerror()
    }

    override fun apiError(errorBody: ResponseBody?) {
        displayApiError(errorBody, tvSignUp)
    }

    override fun apiSuccess(data: SignUpData?) {

        PrefsManager.get().save(AppConstants.APP_TOKEN, "bearer " + data?.accessToken)
        finish()

    }

    override fun onClick(p0: View?) {

        when(p0?.id){


            R.id.tvSignUp->{
                if(validateOk()){
                    if(isNetworkActive()) {
                        val map = HashMap<String, String>()
                        map.put("countryCode", ccp.selectedCountryCodeWithPlus)
                        map.put("phoneNo", etNumber.text.toString().trim())
                        map.put("name", etName.text.toString().trim())
                        map.put("imei", "1")
                        map.put("version", "1")
                        map.put("deviceToken", "1")
                        map.put("deviceType", "ANDROID")
                        map.put("email", etEmail.text.toString().trim())
                        presenter.apiSignUp(map)
                    }else{
                        tvSignUp?.showSnack(R.string.network_error)
                    }
                }

            }
            R.id.tvSignIn->{

               // openLoginFragment()
            }

        }

    }


    private fun validateOk(): Boolean {


        if(etName.text.toString().trim().isEmpty()) {
            etName.error = getString(R.string.name_cannot_empty)
            etName.requestFocus()
            return false
        }
        else if(etEmail.text.toString().trim().isEmpty()){
            etEmail.error = getString(R.string.please_enter_your_email_addr)
            etEmail.requestFocus()
            return false
        } else if(!android.util.Patterns.EMAIL_ADDRESS.matcher(etEmail.text.toString().trim()).matches()){
            etEmail.error = getString(R.string.email_validation_msg)
            etEmail.requestFocus()
            return false
        }
        else if(etNumber.text.toString().trim().isEmpty()) {
            etNumber.error = getString(R.string.phone_cannot_empty)
            etNumber.requestFocus()
            return false
        } else if (!etNumber.text.toString().trim().matches(Regex("[0]*[1-9][0-9]*"))) {
            etNumber.requestFocus()
            etNumber.error = getString(R.string.eror_valid_number)
            return false
        } else if (etNumber.text.toString().startsWith("0"))  {
            etNumber.requestFocus()
            etNumber.error = getString(R.string.eror_valid_number)
            return false
        }
        else if (etNumber.text.toString().trim().length < 6)  {
            etNumber.requestFocus()
            etNumber.error = getString(R.string.length_msg)
            return false
        }
        else if(etPassword.text.toString().isEmpty()){
            etPassword.error = getString(R.string.error_password)
            etPassword.requestFocus()
            return false
        } else if(etPassword.text.toString().length<6){
            etPassword.error = getString(R.string.error_pass_short)
            etPassword.requestFocus()
            return false
        }

        else
            return true
    }


    override fun onDestroy() {
        super.onDestroy()
        presenter.detachView()
    }

}