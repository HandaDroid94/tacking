package com.tackingsample.ui.signup

import com.tackingsample.base.BasePresenterImpl
import com.tackingsample.network.RetrofitClient
import com.tackingsample.network.SignUp
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by prashant on 28/3/18.
 */
class SignupPresenter: BasePresenterImpl<SignupContract.View>(), SignupContract.Presenter  {


    override fun apiSignUp(map: HashMap<String, String>) {

        getView()?.showLoader(true)

        RetrofitClient.getApi().apiRegister(map).enqueue(object: Callback<SignUp> {
            override fun onResponse(call: Call<SignUp>?, response: Response<SignUp>) {
                if(response.isSuccessful){
                    getView()?.apiSuccess(response.body()?.data)

                }
                else{
                    getView()?.apiError(response.errorBody())
                }
                getView()?.showLoader(false)

            }

            override fun onFailure(call: Call<SignUp>?, t: Throwable?) {
                getView()?.showLoader(false)
                getView()?.apiFaliure()
            }
        })

    }



}