package com.tackingsample.ui.login

import com.tackingsample.base.BasePresenterImpl
import com.tackingsample.network.RetrofitClient
import com.tackingsample.network.SignUp
import com.tackingsample.utils.AppConstants
import com.tackingsample.utils.PrefsManager
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by prashant on 28/3/18.
 */
class LoginPresenter: BasePresenterImpl<LoginContract.View>(), LoginContract.Presenter {
    override fun syncApi(map: HashMap<String, String>) {


        getView()?.showLoader(true)

        RetrofitClient.getApi().apiSync(PrefsManager.get().getString(AppConstants.APP_TOKEN,""),
                map).enqueue(object: Callback<SignUp> {
            override fun onResponse(call: Call<SignUp>?, response: Response<SignUp>) {
                if(response.isSuccessful){
                    getView()?.apiSuccessSync(response.body()?.data)
                }
                else{
                    getView()?.apiError(response.errorBody())
                }
                getView()?.showLoader(false)

            }

            override fun onFailure(call: Call<SignUp>?, t: Throwable?) {
                getView()?.showLoader(false)
                getView()?.apiFaliure()
            }
        })
    }


    override fun loginApi(map: HashMap<String, String>) {

        getView()?.showLoader(true)

        RetrofitClient.getApi().apiLogin(map).enqueue(object: Callback<SignUp> {
            override fun onResponse(call: Call<SignUp>?, response: Response<SignUp>) {
                if(response.isSuccessful){
                    getView()?.apiSuccess(response.body()?.data)
                }
                else{
                    getView()?.apiError(response.errorBody())
                }
                getView()?.showLoader(false)

            }

            override fun onFailure(call: Call<SignUp>?, t: Throwable?) {
                getView()?.showLoader(false)
                getView()?.apiFaliure()
            }
        })

    }





}