package com.tackingsample.ui.login

import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.text.method.PasswordTransformationMethod
import android.text.method.TransformationMethod
import android.view.View
import android.widget.CompoundButton
import android.widget.EditText
import android.widget.Toast
import com.tackingsample.R
import com.tackingsample.network.SignUpData
import com.tackingsample.ui.home.HomeActivity
import com.tackingsample.ui.signup.SignupFragment
import com.tackingsample.utils.*
import kotlinx.android.synthetic.main.fragment_login.*
import okhttp3.ResponseBody


/**
 * Created by prashant on 28/3/18.
 */
class LoginFragment : AppCompatActivity(), View.OnClickListener, LoginContract.View {


    override fun apiSuccessSync(data: SignUpData?) {

        startActivity(Intent(this,HomeActivity::class.java))
        finish()

    }


    private val presenter = LoginPresenter()
    private var loadingBox: LoadingDialog? = null
    private var dialog:AlertDialog?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.fragment_login)
        init()
        setClickEvents()
    }


    private fun init() {
        presenter.attachView(this)
        loadingBox = LoadingDialog(this)
        if(!PrefsManager.get().getString(AppConstants.APP_TOKEN,"").equals("")){
            startActivity(Intent(this,HomeActivity::class.java))
            finish()
        }
    }

    private fun setClickEvents() {
        tvSignUp.setOnClickListener(this)
        tvForgot.setOnClickListener(this)
        tvSignIn.setOnClickListener(this)
        tvSkip.setOnClickListener(this)


    }


    override fun showLoader(isLoading: Boolean) {
        loadingBox?.setLoading(isLoading)
    }

    override fun apiFaliure() {
        tvForgot?.showSomeWWerror()
    }

    override fun apiError(errorBody: ResponseBody?) {
        displayApiError(errorBody, tvForgot)
    }


    override fun onClick(p0: View?) {

        when (p0?.id) {




            R.id.tvSignUp -> {
                startActivity(Intent(this,SignupFragment::class.java))
            }

            R.id.tvSignIn -> {
                if (validateOk()) {
                    if (isNetworkActive()) {
                        val map = HashMap<String, String>()
                        map.put("countryCode", ccp.selectedCountryCodeWithPlus)
                        map.put("phoneNo", etNumber.text.toString().trim())
                        map.put("password", etPassword.text.toString().trim())
                        map.put("version", "1")
                        map.put("deviceToken", "1")
                        map.put("deviceType", "ANDROID")
                        presenter.loginApi(map)
                    } else {
                        tvForgot?.showSnack(R.string.network_error)
                    }
                }

            }

        }


    }

    private fun validateOk(): Boolean {

        if(etNumber.text.toString().trim().isEmpty()) {
            etNumber.error = getString(R.string.phone_cannot_empty)
            etNumber.requestFocus()
            return false
        } else if (!etNumber.text.toString().trim().matches(Regex("[0]*[1-9][0-9]*"))) {
            etNumber.requestFocus()
            etNumber.error = getString(R.string.eror_valid_number)
            return false
        } else if (etNumber.text.toString().startsWith("0"))  {
            etNumber.requestFocus()
            etNumber.error = getString(R.string.eror_valid_number)
            return false
        }
        else if (etNumber.text.toString().trim().length < 6)  {
            etNumber.requestFocus()
            etNumber.error = getString(R.string.length_msg)
            return false
        }else if (etPassword.text.toString().isEmpty()) {
            etPassword.error = getString(R.string.error_password)
            etPassword.requestFocus()
            return false
        } else
            return true


    }

    override fun apiSuccess(data: SignUpData?) {

        PrefsManager.get().save(AppConstants.APP_TOKEN, "bearer " + data?.accessToken)

        if(data?.deviceId==null){

            showRefrelDialog()
        }else{

            startActivity(Intent(this,HomeActivity::class.java))
            finish()
        }
    }


    private fun showRefrelDialog() {

        val alert = AlertDialog.Builder(this)
        val edittext =  EditText(this)
        edittext.hint = "Enter IMEMI number"
        // alert.setMessage("Enter Your Message")
        alert.setTitle("Sync your device")
        alert.setView(edittext)

        alert.setPositiveButton(getString(R.string.btn_done),null)
        alert.setNegativeButton(R.string.option_no) { dialog, whichButton ->

        }
        dialog=alert.create()

        dialog?.setOnShowListener(DialogInterface.OnShowListener {
            val button = (dialog as AlertDialog).getButton(AlertDialog.BUTTON_POSITIVE)
            button.setOnClickListener {

                if(edittext.text.toString().trim().isNotEmpty()){
                    val map=HashMap<String,String>()
                    map.put("imei",edittext.text.toString())
                    //map.put("phoneNoCode",arguments!!.getString("countryCode"))
                    map.put("forcefully","true")

                    presenter.syncApi(map)
                    dialog?.dismiss()

                }else{
                    Toast.makeText(this,"IMEI number is required for pursuing further.",Toast.LENGTH_SHORT).show()
                }


            }
        })
        dialog?.show()
    }


}