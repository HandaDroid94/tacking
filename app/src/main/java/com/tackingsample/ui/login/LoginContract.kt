package com.tackingsample.ui.login

import com.tackingsample.base.BaseView
import com.tackingsample.network.SignUpData

/**
 * Created by prashant on 28/3/18.
 */
interface LoginContract {
     interface View: BaseView
    {
       fun apiSuccess(data: SignUpData?)
       fun apiSuccessSync(data: SignUpData?)
    }

    interface Presenter
    {
        fun loginApi(map:HashMap<String,String>)
        fun syncApi(map:HashMap<String,String>)
    }
}