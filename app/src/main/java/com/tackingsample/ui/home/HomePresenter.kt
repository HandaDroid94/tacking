package com.tackingsample.ui.home

import com.tackingsample.base.BasePresenterImpl
import com.tackingsample.network.MapMain
import com.tackingsample.network.RetrofitClient
import com.tackingsample.network.forpolygons.PolygonsMain
import com.tackingsample.utils.AppConstants
import com.tackingsample.utils.PrefsManager
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class HomePresenter: BasePresenterImpl<HomeContract.View>(), HomeContract.Presenter {
    override fun loginApi(map: HashMap<String, String>) {

       // getView()?.showLoader(true)

        RetrofitClient.getApi().apiGetRoute(PrefsManager.get().getString(AppConstants.APP_TOKEN,""),
                map).enqueue(object: Callback<MapMain> {
            override fun onResponse(call: Call<MapMain>?, response: Response<MapMain>) {
                if(response.isSuccessful){
                    getView()?.apiSuccess(response.body()?.data)
                }
                else{
                    getView()?.apiError(response.errorBody())
                }
                getView()?.showLoader(false)

            }

            override fun onFailure(call: Call<MapMain>?, t: Throwable?) {
                getView()?.showLoader(false)
                getView()?.apiFaliure()
            }
        })
    }

    override fun addPolygon(map: HashMap<String, String>) {

        getView()?.showLoader(true)

        RetrofitClient.getApi().apiAddPolygon(PrefsManager.get().getString(AppConstants.APP_TOKEN,""),
                map).enqueue(object: Callback<Any> {
            override fun onResponse(call: Call<Any>?, response: Response<Any>) {
                if(response.isSuccessful){
                    getView()?.apiSuccessPolygon()
                }
                else{
                    getView()?.apiError(response.errorBody())
                }
                getView()?.showLoader(false)

            }

            override fun onFailure(call: Call<Any>?, t: Throwable?) {
                getView()?.showLoader(false)
                getView()?.apiFaliure()
            }
        })
    }

    override fun apiGetPolygons() {

        getView()?.showLoader(true)

        RetrofitClient.getApi().apiGetPolygons(PrefsManager.get().getString(AppConstants.APP_TOKEN,"")
        ).enqueue(object: Callback<PolygonsMain> {
            override fun onResponse(call: Call<PolygonsMain>?, response: Response<PolygonsMain>) {
                if(response.isSuccessful){
                    getView()?.apiSuccessGetPolygon(response.body()?.data)
                }
                else{
                    getView()?.apiError(response.errorBody())
                }
                getView()?.showLoader(false)

            }

            override fun onFailure(call: Call<PolygonsMain>?, t: Throwable?) {
                getView()?.showLoader(false)
                getView()?.apiFaliure()
            }
        })
    }
}