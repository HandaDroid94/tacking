package com.tackingsample.ui.home

import com.tackingsample.base.BaseView
import com.tackingsample.network.MapData
import com.tackingsample.network.forpolygons.PolygonsData

interface HomeContract {

    interface View: BaseView
    {
        fun apiSuccess(data: List<MapData>?)
        fun apiSuccessPolygon()
        fun apiSuccessGetPolygon(data: List<PolygonsData>?)
    }

    interface Presenter
    {
        fun loginApi(map:HashMap<String,String>)
        fun addPolygon(map: HashMap<String, String>)
        fun apiGetPolygons()
    }

}