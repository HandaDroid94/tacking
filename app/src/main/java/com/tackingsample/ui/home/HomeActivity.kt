package com.tackingsample.ui.home

import android.app.Activity
import android.app.DatePickerDialog
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapsInitializer
import com.google.android.gms.maps.OnMapReadyCallback
import com.tackingsample.R
import kotlinx.android.synthetic.main.activity_map.*
import com.tackingsample.network.MapData
import com.tackingsample.utils.*
import okhttp3.ResponseBody
import java.util.*
import com.google.android.gms.maps.CameraUpdateFactory
import android.graphics.Point
import android.location.Address
import android.location.Geocoder
import android.os.Handler
import android.view.MotionEvent
import android.view.View
import android.view.View.OnTouchListener
import com.google.android.gms.location.places.ui.PlaceAutocomplete
import com.google.android.gms.maps.model.*
import com.tackingsample.network.forpolygons.PolygonsData
import kotlin.collections.ArrayList
import kotlin.collections.HashMap
import java.util.Arrays.asList
import com.google.android.gms.maps.model.LatLng




class HomeActivity : AppCompatActivity(), HomeContract.View {


    override fun apiSuccessPolygon() {

        llBottom.visibility = View.GONE
        etName.visibility = View.GONE
        Is_MAP_Moveable = !Is_MAP_Moveable
        `val`.clear()
        tvSave.showSnack("Polygon Added successfully.")
    }

    private var PLACE_AUTOCOMPLETE_REQUEST_CODE = 1
    private var mGoogleMap: GoogleMap? = null
    private val presenter = HomePresenter()
    private var Is_MAP_Moveable = false
    private var animate = false
    private val `val` = ArrayList<LatLng>()
    private var loadingBox: LoadingDialog? = null
    private var handler: Handler? = null
    private var runnable: Runnable? = null
    private var polygon: Polygon? = null
    private var date: DatePickerDialog.OnDateSetListener = DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
        // TODO Auto-generated method stub
        myCalendar?.set(Calendar.YEAR, year)
        myCalendar?.set(Calendar.MONTH, monthOfYear)
        myCalendar?.set(Calendar.DAY_OF_MONTH, dayOfMonth)
        updateLabel()
    }

    private var myCalendar: Calendar? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_map)
        setMapView(savedInstanceState)
        init()
        presenter.apiGetPolygons()
        hitApi()
        if(tvEventDate.text.equals(
                        getFormatFromDate(Calendar.getInstance().time,"dd MMM yyyy"))){
            setHandler()

        }
        clickEvents()

    }

    private fun moveToCurrentLocation(currentLocation: LatLng) {
        mGoogleMap?.moveCamera(CameraUpdateFactory.newLatLngZoom(currentLocation, 15f))
        // Zoom in, animating the camera.
        mGoogleMap?.animateCamera(CameraUpdateFactory.zoomIn())
        // Zoom out to zoom level 10, animating with a duration of 2 seconds.
        mGoogleMap?.animateCamera(CameraUpdateFactory.zoomTo(15f), 1000, null)


    }

    private fun hitApi() {

        if (isNetworkActive()) {
            val map = HashMap<String, String>()
            map.put("startDate", parseDateLocal(
                    getFormatFromDate(myCalendar!!.time, "yyyy MM dd")
                            + " 00:00", "yyyy MM dd hh:mm").time.toString())
            map.put("endDate", parseDateLocal(
                    getFormatFromDate(myCalendar!!.time, "yyyy MM dd")
                            + " 23:59", "yyyy MM dd hh:mm").time.toString())
            presenter.loginApi(map)
        }
    }

    private fun clickEvents() {

        switchMap.setOnClickListener {
            Is_MAP_Moveable = !Is_MAP_Moveable
        }

        tvCancel.setOnClickListener {

            polygon?.remove()


        }

        tvSave.setOnClickListener {

            if (etName.text.toString().isNotEmpty()) {
                var string = ""
                `val`.add(`val`[0])
                if (`val`.isNotEmpty()) {

                    for (i in `val`.indices) {

                        string = string + `val`[i].longitude.toString() + " " +
                                `val`[i].latitude.toString() + ","
                    }
                    val map = HashMap<String, String>()
                    map.put("name", etName.text.toString().trim())
                    map.put("location", string.removeSuffix(","))
                    presenter.addPolygon(map)
                }
            } else {
                tvAdd.showSnack("Add polygon name.")
            }
        }

        frame.setOnTouchListener(object : OnTouchListener {
            override fun onTouch(p0: View?, event: MotionEvent): Boolean {

                val x = event.x
                val y = event.y

                val x_co = Math.round(x)
                val y_co = Math.round(y)

                val projection = mGoogleMap?.getProjection()
                val x_y_points = Point(x_co, y_co)

                val latLng = mGoogleMap?.getProjection()?.fromScreenLocation(x_y_points)


                val eventaction = event.action
                when (eventaction) {
                    MotionEvent.ACTION_DOWN -> {
                        // finger touches the screen
                        if (Is_MAP_Moveable) {
                            `val`.add(latLng ?: LatLng(0.0, 0.0))
                        }
                        // finger leaves the screen
                        if (Is_MAP_Moveable) {
                            Draw_Map()
                        }
                    }

                    MotionEvent.ACTION_MOVE,
                        // finger moves on the screen
                        //                    val.add(new LatLng(latitude, longitude));

                    MotionEvent.ACTION_UP -> if (Is_MAP_Moveable) {
                        Draw_Map()
                    }
                }
                return Is_MAP_Moveable
            }

        })



        tvEventDate.setOnClickListener {

            showDatePicker()

        }
        tvAdd.setOnClickListener {

            if (Is_MAP_Moveable) {
                llBottom.visibility = View.GONE
                etName.visibility = View.GONE
                tvText.visibility = View.GONE
                switchMap.visibility = View.GONE
                tvDateTitle.visibility = View.VISIBLE
                tvEventDate.visibility = View.VISIBLE
            } else {

                llBottom.visibility = View.VISIBLE
                tvText.visibility = View.VISIBLE
                etName.visibility = View.VISIBLE
                switchMap.visibility = View.VISIBLE
                tvDateTitle.visibility = View.GONE
                tvEventDate.visibility = View.GONE
            }

            Is_MAP_Moveable = !Is_MAP_Moveable

        }
    }

    fun Draw_Map() {

        val sydney = `val`.get(`val`.size - 1)
        val markerOptions = MarkerOptions().position(sydney).title("Marker in").snippet("is one of the most popular city in Australia")
        mGoogleMap?.addMarker(markerOptions)

        val rectOptions = PolygonOptions()
        rectOptions.addAll(`val`)
        rectOptions.strokeColor(Color.BLUE)
        rectOptions.strokeWidth(7f)
        rectOptions.fillColor(Color.CYAN)
        polygon = mGoogleMap?.addPolygon(rectOptions)
    }

    private fun showDatePicker() {
        val datePickerDialog = DatePickerDialog(this, date,
                myCalendar!!.get(Calendar.YEAR), myCalendar!!.get(Calendar.MONTH),
                myCalendar!!.get(Calendar.DAY_OF_MONTH))
        datePickerDialog.datePicker.maxDate = System.currentTimeMillis() - 1000
        datePickerDialog.show()
    }

    override fun apiSuccess(data: List<MapData>?) {
        drawPolyline(data)
    }


    private fun init() {
        tvText.setOnClickListener {
            setPlacePicker()
        }
        presenter.attachView(this)
        loadingBox = LoadingDialog(this)
        myCalendar = Calendar.getInstance()
        tvEventDate.text = getFormatFromDate(myCalendar?.time!!, "dd MMM yyyy")
    }

    private fun getAddressFromLatLng(latitude: Double, longitude: Double) {
        val geocoder: Geocoder
        val addresses: List<Address>
        geocoder = Geocoder(this, Locale.getDefault())

        addresses = geocoder.getFromLocation(latitude, longitude, 1) // Here 1 represent max location result to returned, by documents it recommended 1 to 5

        if(!addresses.isEmpty()) {
            val address = addresses.get(0).getAddressLine(0) // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
            tvText.text = address
        }else{
            tvText.text=""
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                val place = PlaceAutocomplete.getPlace(this, data)
                moveCameraToWineries(place.latLng)
                tvText.setText(place.address)
            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                val status = PlaceAutocomplete.getStatus(this, data)
                // TODO: Handle the error.
            } else if (resultCode == Activity.RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }
    }


    private fun setPlacePicker() {
        try {
            val intent=Intent(PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_OVERLAY)
                    .build(this))
            startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);
        } catch (e:Exception ) {
            // TODO: Handle the error.
        }
    }

    private fun moveCameraToWineries(lat: LatLng) {
        val builder = LatLngBounds.Builder()
        builder.include(lat)
        val bounds = builder.build()
        mGoogleMap?.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 12))
    }


    private fun updateLabel() {
        tvEventDate.text = getFormatFromDate(myCalendar?.time!!, "dd MMM yyyy")
        //backendDate =getFormatFromDateUTC(myCalendar?.time!!, AppConstants.DATE_FORMAT_BACKEND)
        hitApi()
        if(!tvEventDate.text.equals(
                        getFormatFromDate(Calendar.getInstance().time,"dd MMM yyyy"))) {

            handler?.removeCallbacks(runnable)
        }else{
            setHandler()
        }
    }

    private fun setHandler() {

        runnable= Runnable {
            hitApi()
            setHandler()
        }
        handler=Handler()
        handler?.postDelayed(runnable,5000)
    }


    private fun drawPolyline(data: List<MapData>?) {

        //mGoogleMap?.clear()
        if (data != null && data.isNotEmpty()) {
            val list = ArrayList<LatLng>()
            for (i in data?.indices!!) {
                list.add(LatLng(data[i]?.location?.coordinates!![1].toDouble(),
                        data[i]?.location?.coordinates!![0].toDouble()))
            }
            val markerOptions = MarkerOptions().position(LatLng(data[0]?.location?.coordinates!![1].toDouble(),
                    data[0]?.location?.coordinates!![0].toDouble()))
                    .title("Marker in").snippet("start route")
            mGoogleMap?.addMarker(markerOptions)
            val markerOption = MarkerOptions().position(LatLng(data[data.size - 1]?.location?.coordinates!![1].toDouble(),
                    data[data.size - 1]?.location?.coordinates!![0].toDouble()))
                    .title("Marker in").snippet("end route")
            mGoogleMap?.addMarker(markerOption)
            val options = PolylineOptions().width(8f).color(Color.BLUE).geodesic(true)
            for (z in 0 until list.size) {
                val point = list.get(z)
                options.add(point)
            }
            mGoogleMap?.addPolyline(options)
            val builder = LatLngBounds.Builder()
            for (latLng in list) {

                builder.include(latLng)
            }

            val bounds = builder.build()

            //BOUND_PADDING is an int to specify padding of bound.. try 100.
            val cu = CameraUpdateFactory.newLatLngBounds(bounds, 0)
            if(!animate)
            mGoogleMap?.animateCamera(cu)
            animate=true
        }
    }


    override fun showLoader(isLoading: Boolean) {
        loadingBox?.setLoading(isLoading)
    }

    override fun apiFaliure() {
        mapView?.showSomeWWerror()
    }

    override fun apiError(errorBody: ResponseBody?) {
        displayApiError(errorBody, mapView)
    }

    override fun apiSuccessGetPolygon(data: List<PolygonsData>?) {


        for (i in data?.indices!!) {
            val list = ArrayList<LatLng>()
//            val items=ArrayList<String>()

            val items = data[i].polygon?.split(",")



//            items.addAll(Arrays.asList(data[i].polygon?.split(","))as ArrayList<String>)



            for (i in items!!.indices) {
                list.add(LatLng(items[i]
                        .substring(items[i].indexOf(" ")).toDouble(),
                        items[i].substring(0,items[i].indexOf(" ")).toDouble()))
            }
            val rectOptions = PolygonOptions()
            rectOptions.addAll(list)
            rectOptions.strokeColor(Color.BLUE)
            rectOptions.strokeWidth(7f)
            rectOptions.fillColor(Color.CYAN)
            polygon = mGoogleMap?.addPolygon(rectOptions)
        }

    }

    private fun setMapView(savedInstanceState: Bundle?) {

        mapView.onCreate(savedInstanceState)
        mapView.onResume()
        try {
            MapsInitializer.initialize(this)

        } catch (e: Exception) {
            e.printStackTrace()
        }
        mapView.getMapAsync(OnMapReadyCallback { googleMap ->
            mGoogleMap = googleMap
            //googleMap.setMyLocationEnabled(true)
            googleMap.setOnCameraChangeListener { cameraPosition ->

            }

        })

    }
}